Rich Castellano, M.D. is an experienced board-certified facial plastic surgeon whose specialties include the latest laser and filler techniques. He has performed over 10,000 cosmetic procedures of the face and neck, making him one of the most experienced facial plastic surgeons in the U.S. today.

Address: 3314 Henderson Blvd, #201, Tampa, FL 33609, USA

Phone: 877-346-2435

Website: https://imagelift.com
